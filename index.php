<?php
 require_once('./connexion.php');

 $sql = "SELECT * FROM contact ORDER BY date_heure DESC LIMIT 40";
 $query = $db->prepare($sql);
 $query->execute();

 $contacts = $query->fetchAll();

?>
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Liste de Contacts</title>
</head>
<body>
    <h1>Liste de Contacts</h1>
    <a href="./addContact.php">Ajouter un contact</a>
    <table>
        <thead>
            <tr>
                <th>Id</th>
                <th>Contact</th>
                <th>Email</th>
                <th>Tél</th>
                <th>Objet</th>
                <th>Message</th>
                <th>Date</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach($contacts as $contact){ ?>
                <tr>
                    <td><?= $contact['id']; ?></td>
                    <td><?= $contact['nom'] .' '. $contact['prenom']; ?></td>
                    <td><a href="mailto:<?= $contact['email']; ?>"><?= $contact['email']; ?></a></td>
                    <td><a href="tel:<?= $contact['telephone']; ?>"><?= $contact['telephone']; ?></a></td>
                    <td><?= $contact['objet']; ?></td>
                    <td><?= $contact['message']; ?></td>
                    <td><?= date( 'j/m/y H:i', strtotime($contact['date_heure'])); ?></td>
                    <td></td>
                </tr>
            <?php } ?>
        </tbody>
    </table>
</body>
</html>