<?php
require_once('./connexion.php');
$erreur = "";

if (isset($_POST['submit'])){

    if (isset($_POST['nom']) && $_POST['nom'] != '' && isset($_POST['prenom']) && $_POST['prenom'] ){
        $sql = "INSERT INTO contact(nom, prenom, email, telephone, objet, `message`, date_heure, ip) 
        VALUES (:nom, :prenom, :email, :telephone, :objet, :message, :date_heure, :ip);";
        
        $nom = htmlspecialchars(trim($_POST['nom']));
        $prenom = htmlspecialchars(trim($_POST['prenom'])) ;
        $email = (isset($_POST['email']) && $_POST['email'] != '') ? htmlspecialchars(trim($_POST['email'])) : '';
        $tel = (isset($_POST['tel']) && $_POST['tel'] != '') ? htmlspecialchars(trim($_POST['tel'])) : '';
        $objet = (isset($_POST['objet']) && $_POST['objet'] != '') ? htmlspecialchars(trim($_POST['objet'])) : null;
        $message = (isset($_POST['message']) && $_POST['message'] != '') ? htmlspecialchars(trim($_POST['message'])) : '';
        $date_heure = date('Y-m-d H:i:s');
        $ip = isset($_SERVER['HTTP_CLIENT_IP']) ? $_SERVER['HTTP_CLIENT_IP'] : '';

        $query = $db->prepare($sql);
        $query->execute([
            "nom" => $nom, 
            "prenom" => $prenom, 
            "email" => $email, 
            "telephone" => $tel, 
            "objet" => $objet, 
            "message" => $message, 
            "date_heure" => $date_heure,
            "ip" => $ip
        ]);

        header("Location:index.php");

    } 
    else {
        $erreur = "<p>C'est pas bien de changer les formulaires Rien n'a été sauvegardé.</p>"; 
    } 

    
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=1, initial-scale=1.0">
    <title>Ajout de contact</title>
</head>
<body>
    <?= $erreur ?>
    <form action="/addContact.php" method="POST">
        <input type="text" name="prenom" placeholder="Prénom" required>
        <input type="text" name="nom" placeholder="Nom" required>
        <input type="email" name="email" placeholder="Email" required>
        <input type="tel" name="tel" placeholder="Téléphone" required>
        <input type="text" name="objet" placeholder="Sujet">
        <textarea name="message" required></textarea>
        <input type="submit" name="submit" value="Envoyer">
    </form>
</body>
</html>
